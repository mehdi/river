/* global Vue */

import LocalStorage from './modules/localstorage'
import App from './app.vue'

Vue.use(LocalStorage, { prefix: 'river-config-' })

window.app = new Vue({
  el: '#app',
  render: h => h(App),
  localStorage: {
    filesShowUseless: {
      type: Boolean,
      default: false
    },
    seriesShowLast: {
      type: Boolean,
      default: true
    }
  }
})
