export const sanitizePath = (filePath) => ('/' + filePath).replace(/\/+/g, '/')

export const encode = (filePath) => filePath.split('/').map(encodeURIComponent).join('/')

export const decode = (filePath) => filePath.split('/').map(decodeURIComponent).join('/')

export const parseQueryString = queryString => queryString.split('&')
  .map(str => str.split('='))
  .reduce((obj, pair) => Object.assign(obj, { [pair[0]]: pair.length === 1 ? true : pair[1] }), {})

export const generateQueryString = query => {
  const keys = Object.keys(query).filter(key => query[key])
  if (keys.length === 0) return ''
  else return '?' + keys.map(key => query[key] === true ? key : `${key}=${query[key]}`).join('&')
}

// This is all disabled because specific behaviour would be shit to reproduce (nothing for empty params for example)
// const fromEntries = entries => Array.from(entries)
//  .reduce((obj, [key, val]) => ({ ...obj, [key]: val }), {})
// const generateQueryString = (query) => (new URLSearchParams(query)).toString()
// const parseQueryString = (queryString) => fromEntries((new URLSearchParams(queryString)).entries())

export const getExt = (name) => name.includes('.') ? name.split('.').pop() : ''

export const getBaseName = (name) => name.includes('.') ? name.slice(0, -getExt(name).length - 1) : name

export const padLeft = (nr, n, str = '0') => new Array(n - String(nr).length + 1).join(str) + nr

export const validateMmvPath = (input) => input && !input.startsWith('-') && !input.startsWith('/') && !input.startsWith('.')

export const validateMmvFrom = input => validateMmvPath(input)

export const validateMmvTo = input => validateMmvPath(input) &&
  !input.endsWith('#') &&
  !input.includes('*') &&
  !input.includes('?')

export const formatDuration = time => {
  time = Math.round(time)
  const seconds = padLeft(time % 60, 2)
  const minutes = padLeft(Math.floor((time % 3600) / 60), 2)
  const hours = Math.floor(time / 3600)
  return `${hours ? padLeft(hours, 2) + ':' : ''}${minutes}:${seconds}`
}

export const padFloat = (x, decimalPlaces = 1) => {
  const exponent = (10 ** decimalPlaces)
  const y = Math.round(Math.abs(x) * exponent)
  const decimals = y % exponent
  const int = Math.round((y - decimals) / exponent)
  return `${Math.sign(x) === -1 ? '-' : ''}${int}.${decimals}`
}
