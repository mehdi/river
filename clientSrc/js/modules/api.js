import { sanitizePath } from './utils'

const fetchOptions = async ({ body, headers = {}, method = 'POST' } = {}) => {
  /* // TODO
  let csrfToken = getCookie('csrftoken')
  if (csrfToken === '') { // If no CSRF token, we do a dummy call to get one
    await fetch('/api/status/', {
      method: 'POST'
    })
    csrfToken = getCookie('csrftoken')
  }
  */
  return {
    method,
    credentials: 'same-origin',
    headers: {
      // 'X-CSRFToken': csrfToken
      ...headers
    },
    body
  }
}

const jsonFetchOptions = obj => fetchOptions({
  body: JSON.stringify(obj),
  headers: { 'Content-Type': 'application/json' }
})

export const logout = () => {
  window.location.href = '/api/logout'
}

export const login = async (username, password) => {
  const response = await fetch(
    '/api/login',
    await jsonFetchOptions({ username, password })
  )
  if (response.status === 401) {
    throw new Error('Unauthorized')
  }
  return response.json()
}

export const checkLogin = async () => {
  const response = await fetch(
    '/api/check-login',
    await fetchOptions({ method: 'GET' })
  )
  if (response.status === 401) return null
  else if (response.status === 200) return response.json()
  else throw new Error('Unexpected error')
}

const goToLogin = () => {
  const currentHash = window.location.hash.replace(/^#/, '')
  if (!currentHash.startsWith('login')) {
    window.location.hash = `#login?redirect=#${currentHash}`
  }
}

export const move = async (from, to) => {
  const response = await fetch(
    '/api/files/move',
    await jsonFetchOptions({ from, to })
  )
  if (response.status === 401) {
    setTimeout(goToLogin, 100)
    throw new Error('Unauthorized')
  }
  if (response.status === 409) throw new Error('Already exists')
  if (response.status !== 201) throw new Error('Error moving file: ' + response.status)
}

export const createDirectory = async (dirPath) => {
  const response = await fetch(
    '/api/files/create_directory',
    await jsonFetchOptions({ dirPath })
  )
  if (response.statusCode === 401) {
    setTimeout(goToLogin, 100)
    throw new Error('Unauthorized')
  }
  if (response.status === 409) throw new Error('Directory already exists')
  if (response.status !== 201) throw new Error('Error creating directory: ' + response.status)
}

export const del = async (filePath) => { // TODO: problems with URL encoding ?
  const response = await fetch(
    '/api/files/delete',
    await jsonFetchOptions({ filePath })
  )
  if (response.status === 401) {
    setTimeout(goToLogin, 100)
    throw new Error('Unauthorized')
  }
  if (response.status !== 200) throw new Error('Error deleting file: ' + response.status)
  return response.json()
}

export const mmv = async (dirPath, from, to, dryRun = false) => {
  const response = await fetch(
    '/api/files/mmv',
    await jsonFetchOptions({ dirPath, from, to, dryRun })
  )
  if (response.status === 401) {
    setTimeout(goToLogin, 100)
    throw new Error('Unauthorized')
  }
  const data = await response.json()
  if (!response.ok) {
    if (data.err && data.stdout) {
      const output = (data.stdout + (data.stderr || ''))
        .replace(/\n/g, '\n\n')
        .trim()
      throw new Error(output)
    } else {
      throw new Error('Failed mmv:' + response.status + JSON.stringify(data))
    }
  } else {
    return data.stdout
      .replace(/\n/g, '\n\n')
      .trim()
  }
}

export const upload = async (path, file) => {
  const filePath = sanitizePath(path + '/' + file.name)

  const formData = new FormData()
  formData.append('file', file)
  formData.append('filePath', filePath)

  const response = await fetch(
    '/api/files/upload',
    await fetchOptions({ body: formData })
  )
  if (response.status === 401) {
    setTimeout(goToLogin, 100)
    throw new Error('Unauthorized')
  }
  if (response.status !== 201) throw new Error('Error uploading file: ' + response.status)
}

export const listDir = async (dirPath) => {
  const response = await fetch(
    '/api/files/list',
    await jsonFetchOptions({ dirPath })
  )
  if (response.status === 401) {
    setTimeout(goToLogin, 100)
    throw new Error('Unauthorized')
  }
  if (response.status !== 222) throw new Error('Error listing directory: ' + response.status)

  const data = await response.json()
  return data.entries
}

export const getMoviesList = async () => {
  const response = await fetch(
    '/api/movies/list_movies',
    await fetchOptions()
  )

  if (response.status === 401) {
    setTimeout(goToLogin, 100)
    throw new Error('Unauthorized')
  }
  if (response.status !== 222) throw new Error('Error getting movies list: ' + response.status)

  const data = await response.json()
  return data.movies
}

export const getShowsList = async () => {
  const response = await fetch(
    '/api/series/list_shows',
    await fetchOptions()
  )

  if (response.status === 401) {
    setTimeout(goToLogin, 100)
    throw new Error('Unauthorized')
  }
  if (response.status !== 222) throw new Error('Error getting shows list: ' + response.status)

  const data = await response.json()
  return data.shows
}

export const getShowDetails = async (dirName) => {
  const response = await fetch(
    '/api/series/show_details',
    await jsonFetchOptions({ dirName })
  )

  if (response.status === 401) {
    setTimeout(goToLogin, 100)
    throw new Error('Unauthorized')
  }
  if (response.status === 404) throw new Error('Not found')
  if (response.status !== 222) throw new Error('Error getting shows list: ' + response.status)

  const data = await response.json()
  return data
}
