const LS = {
  set: (key, value) => {
    window.localStorage.setItem(key, JSON.stringify(value))
  },
  get: (key) => {
    const item = window.localStorage.getItem(key)
    if (item) {
      return JSON.parse(item)
    } else {
      throw new Error(`Couldn't get key ${key} in storage`)
    }
  }
}

let Vue_

export class StorageStore {
  constructor (keys, prefix) {
    if (!(this instanceof StorageStore)) throw new Error('Store must be called with the new operator.')
    if (!Vue_) throw new Error('You must install the localStorage store first')
    if (!keys || !prefix) throw new Error('Invalid arguments')

    this.keys = keys
    this.prefix = prefix
    const state = {}

    Object.keys(keys).forEach(key => {
      try {
        state[key] = LS.get(this.prefix + key)
      } catch (e) {
        state[key] = keys[key].default
        LS.set(this.prefix + key, keys[key].default)
      }
    })

    this._vm = new Vue_({
      data: {
        $$state: state
      }
    })
  }

  get (key) {
    return this._vm._data.$$state[key]
  }

  set (key, value) {
    if (!this.keys[key]) throw new Error('Unknown key')
    const type = this.keys[key].type
    if (type === String) {
      value = String(value)
    } else if (type === Number) {
      if (isNaN(value)) throw new Error('Not a valid number')
    } else if (type === Object) {
      value = { ...value }
    } else if (type === Array) {
      value = Array.from(value)
    } else if (type === Boolean) {
      value = Boolean(value)
    } else if (typeof type === 'function') {
      if (!type(value)) throw new Error('Validation function failed')
    }
    LS.set(this.prefix + key, value)
    this._vm._data.$$state[key] = value
    return value
  }
}

export const storageComputed = (keys) => {
  const computed = {}
  keys.forEach(key => {
    computed[key] = {
      get () {
        return this[`$${VueStorage.name}`].get(key)
      },
      set (v) {
        this[`$${VueStorage.name}`].set(key, v)
        return v
      }
    }
  })
  return computed
}

const VueStorage = {
  name: 'localStorage',
  prefix: '',
  install (_Vue, options = {}) {
    Vue_ = _Vue
    if (options.name) VueStorage.name = options.name
    if (options.prefix) VueStorage.prefix = options.prefix
    const name = VueStorage.name

    Vue_.mixin({
      beforeCreate () {
        const options = this.$options
        if (options[name]) {
          this[`$${name}`] = new StorageStore(this.$options[name], VueStorage.prefix)
        } else if (options.parent && options.parent[`$${name}`]) {
          this[`$${name}`] = options.parent[`$${name}`]
        }
      }
    })
  },
  storageComputed,
  StorageStore,
  LS
}

export default VueStorage
