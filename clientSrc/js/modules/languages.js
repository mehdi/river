export const languages = {
  ar: 'العربية',
  de: 'Deutsch',
  en: 'English',
  es: 'Español',
  fr: 'Français',
  it: 'Italiano',
  nl: 'Nederlands',
  ru: 'Русский',
  sv: 'Svenska',
  zh: '中文'
}

export const emojis = { // There are emojis in here
  ar: '🇸🇦',
  de: '🇩🇪',
  en: '🇬🇧',
  es: '🇪🇸',
  fr: '🇫🇷',
  it: '🇮🇹',
  nl: '🇳🇱',
  ru: '🇷🇺',
  sv: '🇸🇪',
  zh: '🇨🇳'
}

const langMap = {
  eng: 'en',
  fra: 'fr',
  ita: 'it'
}

export const parseLang = (file) => {
  const result = /^.+\.([a-z]{2,3})\.(?:srt|ass|sub|vtt)$/.exec(file.toLowerCase())
  if (result && result[1]) return langMap[result[1]] || result[1]
}
