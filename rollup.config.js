import babel from 'rollup-plugin-babel'
import vue from 'rollup-plugin-vue'
import nodeResolve from 'rollup-plugin-node-resolve'
import commonJs from 'rollup-plugin-commonjs'

process.env.BABEL_ENV = 'rollup'

export default {
  input: 'clientSrc/js/index.js',
  plugins: [
    vue({
      sourceRoot: 'clientSrc/',
      template: {
        transformAssetUrls: false
      },
      style: {
        preprocessOptions: {
          scss: {
            sourceMap: false
          }
        }
      }
    }),
    nodeResolve({
      mainFields: ['browser', 'module', 'main']
    }),
    commonJs({
      include: 'node_modules/**'
    }),
    babel({
      exclude: 'node_modules/**',
      presets: [
        ['@babel/preset-env', {
          useBuiltIns: 'usage',
          corejs: 3,
          modules: false,
          targets: { browsers: ['last 3 Chrome versions', 'last 3 Firefox version'] }
        }]
      ]
    })
  ],
  output: [
    { file: 'client/js/index.js', format: 'iife' }
  ]
}
