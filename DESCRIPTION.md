River is an app that packages together:
- Jellyfin, a streaming platform, like your own self-hosted Netflix
- SickChill, to auto-download TV shows
- CouchPotato, to auto-download movies
- Transmission, to handle actually downloading things
- A simple file manager, when you need to handle things manually (create directories, move files by drag&drop, upload, rename, delete, download files, stream videos)

Basically, just add the TV Shows you want in SickChill, add the movies you want in CouchPotato, and watch everything on Jellyfin.
