#!/usr/bin/env node
'use strict'

const fs = require('fs').promises
const path = require('path')
// const { convertToMp4 } = require('../convert/utils')
const childProcess = require('child_process')
const { promisify } = require('util')

const execFile = promisify(childProcess.execFile)

const watchDir = '/app/data/files/Downloading/'
const destDir = '/app/data/files/Downloaded/'

const processFile = async (inputPath) => {
  const outputPath = inputPath.replace(watchDir, destDir)
  const outputDir = path.dirname(outputPath)
  await fs.mkdir(outputDir, { recursive: true })
  // if (inputPath.toLowerCase().endsWith('.mkv')) { // Commenting out the auto-remuxing for now, it's not really useful anymore with jellyfin
  //   try { // if mkv, try remuxing it to mp4
  //     await convertToMp4(inputPath, { outputDir })
  //   } catch (err) { // and fallback to hardlink if it fails
  //     console.error(`Error while converting ${inputPath} to mp4:`, err)
  //     console.error('Moving it instead')
  //     await fs.link(inputPath, outputPath)
  //   }
  // } else { // if not, just hardlink
  await fs.link(inputPath, outputPath)
  // }
}

const processPath = async (toProcess) => {
  const result = await fs.stat(toProcess)

  if (result.isDirectory()) {
    const dirList = await fs.readdir(toProcess)
    for (const entry of dirList) {
      await processPath(path.join(toProcess, entry))
    }
  } else if (result.isFile()) {
    await processFile(toProcess)
  } // else, it's a weird thing, ignore it
}

const torrentDir = process.env.TR_TORRENT_DIR
const torrentName = process.env.TR_TORRENT_NAME

if (!torrentDir || !torrentName) {
  console.error('Error: you must call this script from transmission. TR_TORRENT_DIR & TR_TORRENT_NAME were not passed')
  process.exit(1)
}

const torrentPath = path.join(torrentDir, torrentName)

if (!torrentPath.startsWith(watchDir)) {
  console.log('This torrent is not in the watching directory. Skipping.')
} else {
  // TODO: move this to an API call, so we can finally have some logs
  processPath(torrentPath)
    .then(() => execFile('/app/code/transmission/purge.sh'))
}
