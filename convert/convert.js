'use strict'

// This is a script to manually convert files

const utils = require('./utils')
const fs = require('fs')
const { promisify } = require('util')

const inputFiles = process.argv.slice(2)
const rm = promisify(fs.unlink)

const exists = (input) => {
  try {
    return !!fs.statSync(input)
  } catch (err) {
    return false
  }
}

if (inputFiles.length === 0) {
  console.error('Input file not given')
  console.error('Usage: node ./convert.js inputMkvFile [...inputMkvFiles]')
  process.exit(1)
}

for (const inputFile of inputFiles) {
  if (!exists(inputFile)) {
    console.error(`Input file ${inputFiles} does not exist`)
    process.exit(1)
  }
}
let promise = Promise.resolve()
let converted = 0
let failed = 0

for (const inputFile of inputFiles) {
  promise = promise.then(() => utils.convertToMp4(inputFile)
    .then(() => {
      console.log(inputFile + ' converted')
      converted++
      return rm(inputFile)
    })
    .catch(err => {
      failed++
      console.error(`Error while converting ${inputFiles} to mp4:`, err)
    }))
}

promise = promise.then(() => {
  if (converted) {
    console.log(`All done. Converted ${converted} files successfully${failed ? `, ${failed} failed.` : '.'}`)
  }
})
