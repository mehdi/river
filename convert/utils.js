'use strict'

const path = require('path')
const mkvSubtitleExtractor = require('mkv-subtitle-extractor')
const childProcess = require('child_process')
const fs = require('fs')
const { promisify } = require('util')

const execFile = promisify(childProcess.execFile)
const rename = promisify(fs.rename)

/* // unused for now, so let's comment it
const getFormat = async (filePath) => {
  const res = await execFile('ffprobe', [
    '-v',
    'error',
    '-show_entries',
    'format=format_name',
    '-of',
    'default=noprint_wrappers=1:nokey=1',
    filePath
  ])
  return res.stdout.trim() // remove ending \n
}
*/

const fileBaseName = filePath => path.basename(filePath, path.extname(filePath))

const getCodec = async (filePath, stream = 'v:0') => {
  const res = await execFile('ffprobe', [
    '-v',
    'error',
    '-select_streams',
    stream,
    '-show_entries',
    'stream=codec_name',
    '-of',
    'default=noprint_wrappers=1:nokey=1',
    filePath
  ])
  return res.stdout.trim() // remove ending \n
}

const compatbileVideoCodecs = ['h264']
const compatibleAudioCodecs = ['aac', 'ac3', 'eac3'] // TODO: is it really necessary to have a list of convertible audio codecs ?

const canRemuxToMp4 = async (filePath) => {
  try {
    const videoCodec = await getCodec(filePath, 'v:0')
    const audioCodec = await getCodec(filePath, 'a:0')
    return compatbileVideoCodecs.includes(videoCodec) && compatibleAudioCodecs.includes(audioCodec)
  } catch (err) {
    console.warn(`Error while checking file codecs for ${filePath} :`, err)
    return false
  }
}

const convertToMp4 = async (inputPath, { outputDir, force = false } = {}) => {
  const videoCodec = await getCodec(inputPath, 'v:0')
  const audioCodec = await getCodec(inputPath, 'a:0')
  if (!compatbileVideoCodecs.includes(videoCodec) || !compatibleAudioCodecs.includes(audioCodec)) {
    if (!force) throw new Error(`Cannot convert file ${inputPath} . Codecs: ${videoCodec}/${audioCodec}`)
  }
  outputDir = outputDir || path.dirname(inputPath)
  const outputPath = path.join(
    outputDir,
    `${fileBaseName(inputPath)}.mp4`
  )
  console.log(`Converting ${inputPath} to ${outputPath}`)
  await execFile('nice', [
    '-n', '10',
    'ffmpeg',
    '-i', inputPath,
    '-c:v', videoCodec === 'h264' ? 'copy' : 'h264',
    '-c:a', audioCodec === 'aac' ? 'copy' : 'aac',
    '-f', 'mp4', // force mp4 output format, as putting '.part' at the end of the filename prevents autodetection
    `${outputPath}.part`
  ])
  await rename(`${outputPath}.part`, outputPath)

  if (inputPath.toLowerCase().endsWith('.mkv')) mkvSubtitleExtractor(inputPath, outputDir)
}

module.exports = { getCodec, canRemuxToMp4, convertToMp4 }
