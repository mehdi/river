#!/bin/bash

set -eu

export NODE_ENV=production

# Copy the default transmission settings to a user-modifiable place
if [ ! -f /app/data/transmission-daemon/transmission.settings.json ]; then
  mkdir -p /app/data/transmission-daemon/
  cp /app/code/transmission/settings.json /app/data/transmission-daemon/transmission.settings.json
fi

# Ensure Sickchill settings exist
if [ ! -f /app/data/sickchill.ini ]; then
  if [ -f /app/data/sickrage.ini ]; then # if old sickrage settings exist, move & modify them
    mv /app/data/sickrage.ini /app/data/sickchill.ini
    sed -e 's/SickRage/SickChill/' -e 's/sickrage/sickchill/' -i /app/data/sickchill.ini
  else # else use default conf
    cp /app/code/sickchill.ini /app/data/sickchill.ini
  fi
fi

# Setting up SickChill data dir
if [ ! -d /app/data/sickchill ]; then
  if [ -d /app/data/sickrage ]; then # if old sickrage data dir exists, move it
    mv /app/data/sickrage /app/data/sickchill
  else # else create empty dir
    mkdir -p /app/data/sickchill
  fi
fi
# Setting up SickChill UI cache
mkdir -p /app/data/sickchill_cache
# Remove SickChill DB cache: not retro-compatible with old version... we loose it on app restart  ¯\_(ツ)_/¯
rm -f /app/data/sickchill/cache.db

# Setting up CouchPotato data dir
if [ ! -f /app/data/couchpotato/settings.conf ]; then
  mkdir -p /app/data/couchpotato
  cp /app/code/couchpotato/settings.conf /app/data/couchpotato/settings.conf
fi

# Jellyfin LDAP: If the old ldap plugin was installed, remove it and install a symlink
if [ -d /app/data/jellyfin/data/plugins/LDAP\ Authentication ]; then
  rm -r /app/data/jellyfin/data/plugins/LDAP\ Authentication
  ln -s /app/code/jellyfin/jellyfin_ldap /app/data/jellyfin/data/plugins/LDAP\ Authentication
fi
# Setting up Jellyfin directories
if [ ! -d /app/data/jellyfin ]; then
  mkdir -p /app/data/jellyfin/data/data
  mkdir -p /app/data/jellyfin/config
  mkdir -p /app/data/jellyfin/log

  mkdir -p /app/data/jellyfin/data/plugins
  ln -s /app/code/jellyfin/jellyfin_ldap /app/data/jellyfin/data/plugins/LDAP\ Authentication

  cp /app/code/jellyfin/system.xml /app/data/jellyfin/config/system.xml
  cp /app/code/jellyfin/migrations.xml /app/data/jellyfin/config/migrations.xml # Make jellyfin think migrations have been done, because this is a fresh install, but IsStartupWizardCompleted is set to true in system.xml. Otherwise, migrations fail because old DB files do not exist (activitylog.db & users.db)
  cp /app/code/jellyfin/library.db /app/data/jellyfin/data/data/library.db
fi
mkdir -p /tmp/jellyfin

# Creating a secret for passport's sessions
if [ ! -f /app/data/session.secret ]; then
  dd if=/dev/urandom bs=256 count=1 | base64 > /app/data/session.secret
fi

# Setting up download directories
mkdir -p /app/data/files/Series/
mkdir -p /app/data/files/Downloading/Series/
mkdir -p /app/data/files/Downloaded/Series/
mkdir -p /app/data/files/Movies/
mkdir -p /app/data/files/Downloading/Movies/
mkdir -p /app/data/files/Downloaded/Movies/

# Writing config files
node ./writeNginxConfig.js
node ./transmission/writeTransmissionConfig.js
node ./jellyfin/writeLdapAuthXml.js

chown -R cloudron:cloudron /app/data /tmp /run

echo "Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i River
