FROM node:12 AS BUILDER
RUN mkdir -p /app/code
WORKDIR /app/code
COPY package.json package-lock.json rollup.config.js /app/code/
RUN npm ci
COPY clientSrc /app/code/clientSrc
COPY .gitignore .eslintrc.js /app/code/
RUN npm run build
RUN rm .gitignore .eslintrc.js

FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4
MAINTAINER Mehdi Kouhen <arantes555@gmail.com>

ARG NODE_VERSION="12.18.4"
ARG NGINX_VERSION="1.19.2"
ARG SICKCHILL_VERSION="v2020.09.21-2"
ARG COUCHPOTATO_VERSION="7260c12f72447ddb6f062367c6dfbda03ecd4e9c"
ARG JELLYFIN_VERSION="10.6.4-1"
ARG JELLYFIN_LDAP_VERSION="9.0.0.0"

RUN mkdir -p /usr/local/node-${NODE_VERSION}
RUN curl -L "https://nodejs.org/download/release/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz" \
    | tar -xz --strip-components=1 -C /usr/local/node-${NODE_VERSION}

ENV PATH=/usr/local/node-${NODE_VERSION}/bin:$PATH

RUN apt-get update -y
RUN apt-get remove -y nginx-full && apt-get autoremove -y
RUN apt-get install -y transmission-daemon transmission-cli unrar-free mmv ffmpeg x264 x265 iotop libpcre3 libpcre3-dev

RUN mkdir -p /app/code

## Getting nginx
RUN mkdir -p /tmp/nginx
WORKDIR /tmp/nginx
RUN wget "https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz" -O - \
    | tar -xz --strip-components=1 -C /tmp/nginx
RUN ./configure \
    --with-http_auth_request_module \
    --modules-path=/usr/local/nginx/modules \
    --conf-path=/run/nginx.conf \
    --pid-path=/run/nginx.pid \
    --error-log-path=/run/nginx.error.log \
    --build=cloudron-river
RUN make install

RUN rm -rf /tmp/nginx

## SickChill
RUN mkdir -p /app/code/sickchill
RUN curl -L "https://github.com/SickChill/SickChill/archive/${SICKCHILL_VERSION}.tar.gz" \
    | tar -xz --strip-components=1 -C /app/code/sickchill
RUN ln -s /app/data/sickchill_cache /app/code/sickchill/sickchill/gui/slick/cache

## CouchPotato
RUN mkdir -p /app/code/couchpotato
RUN pip install --upgrade pyopenssl lxml
RUN curl -L "https://github.com/CouchPotato/CouchPotatoServer/archive/${COUCHPOTATO_VERSION}.tar.gz" \
    | tar -xz --strip-components=1 -C /app/code/couchpotato

## Jellyfin
RUN apt-get install -y apt-transport-https unzip
RUN add-apt-repository universe
RUN wget -O - https://repo.jellyfin.org/ubuntu/jellyfin_team.gpg.key | sudo apt-key add -
RUN echo "deb [arch=$( dpkg --print-architecture )] https://repo.jellyfin.org/ubuntu $( lsb_release -c -s ) main" | tee /etc/apt/sources.list.d/jellyfin.list
RUN apt-get update -y
RUN apt-get install -y "jellyfin=${JELLYFIN_VERSION}"
RUN mkdir -p /app/code/jellyfin
RUN wget "https://repo.jellyfin.org/releases/plugin/ldap-authentication/ldap-authentication_${JELLYFIN_LDAP_VERSION}.zip" -O /app/code/jellyfin/jellyfin_ldap.zip
RUN unzip /app/code/jellyfin/jellyfin_ldap.zip -d /app/code/jellyfin/jellyfin_ldap
RUN rm /app/code/jellyfin/jellyfin_ldap.zip
# Workaround for https://github.com/jellyfin/jellyfin/issues/3638 / https://github.com/jellyfin/jellyfin/issues/3956
RUN ln -s /usr/share/jellyfin/web/ /usr/lib/jellyfin/bin/jellyfin-web

## Installing file-manager interface && packaging scripts
WORKDIR /app/code
COPY package.json package-lock.json /app/code/
RUN npm ci --production
COPY server /app/code/server
COPY --from=BUILDER /app/code/client /app/code/client

COPY start.sh /app/code/

## Config
COPY sickchill.ini /app/code/sickchill.ini
COPY nginx/writeNginxConfig.js /app/code/

## Transmission utils
COPY transmission /app/code/transmission
RUN chmod +x start.sh transmission/*.js transmission/*.sh

## Jellyfin utils
COPY jellyfin /app/code/jellyfin

## Couchpotato utils
COPY couchpotato /app/code/couchpotato

## Conversion utils
COPY convert /app/code/convert

## Supervisor
COPY supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
RUN sed -e 's,^chmod=.*$,chmod=0760\nchown=cloudron:cloudron,' -i /etc/supervisor/supervisord.conf

EXPOSE 8000

CMD [ "/app/code/start.sh" ]
