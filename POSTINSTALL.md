The different services available are:
- files interface: `/`
- Jellyfin: `/jellyfin/`
- Transmission web interface (admin only): `/transmission/web/`
- SickChill (admin only): `/sickchill/`
- Couchpotato (admin only): `/couchpotato/`
