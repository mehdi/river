'use strict'

const fs = require('fs').promises
const path = require('path')
const tXml = require('txml')
const express = require('express')
const HttpError = require('connect-lastmile').HttpError

module.exports = basePath => {
  const moviesBasePath = path.resolve(path.join(basePath, 'Movies'))

  const router = new express.Router()

  const getAbsolutePath = (filePath) => {
    const absoluteFilePath = path.resolve(path.join(moviesBasePath, filePath))

    if (!absoluteFilePath.startsWith(moviesBasePath)) return null
    return absoluteFilePath
  }

  const parseXml = async (filePath) => {
    const data = await fs.readFile(filePath, 'utf8')
    const parsed = tXml.simplify(tXml(data, {})) // the simplify option appears broken...
    return parsed['?xml']
  }

  const parseMovieXml = async (filePath) => {
    const parsed = await parseXml(filePath)
    const metadata = parsed.movie
    return {
      title: metadata.title,
      rating: parseFloat(metadata.rating),
      year: parseInt(metadata.year),
      plot: metadata.plot,
      id: metadata.id,
      director: metadata.director,
      runtime: metadata.runtime,
      genre: typeof metadata.genre === 'string' ? [metadata.genre] : Array.from(metadata.genre || [])
    }
  }

  const getMovie = async (dirName) => {
    const movieDirPath = getAbsolutePath(dirName)
    if (!movieDirPath) return null
    const movieDir = await fs.readdir(movieDirPath, { withFileTypes: true })
    const movieDirEntries = movieDir.map(dirent => dirent.name)
    const nfoFile = movieDirEntries.find(e => e.endsWith('.nfo'))
    if (!nfoFile) return null
    const filePath = movieDirEntries.find(e => /\.(mp4|mkv|avi)$/.test(e))
    if (!filePath) return null
    const metadata = await parseMovieXml(path.join(movieDirPath, nfoFile))
    return {
      dirName,
      path: movieDirPath,
      filePath,
      landscape: movieDirEntries.includes('landscape.jpg') ? 'landscape.jpg' : null,
      fanart: movieDirEntries.find(e => e.endsWith('-fanart.jpg')),
      thumbnail: movieDirEntries.find(e => e.endsWith('.tbn')),
      title: metadata.title,
      rating: metadata.rating,
      year: metadata.year,
      plot: metadata.plot,
      id: metadata.id,
      director: metadata.director,
      runtime: metadata.runtime,
      genre: metadata.genre,
      subtitles: movieDirEntries.filter(e => /\.(srt|vtt)$/.test(e))
    }
  }

  router.post('/list_movies', async (req, res, next) => {
    try {
      const movieDirs = (await fs.readdir(moviesBasePath, { withFileTypes: true }))
        .filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name)
      const movies = (await Promise.all(movieDirs.map(entry => getMovie(entry))))
        .filter(item => item) // filter out null
        .map(movie => ({
          ...movie,
          path: movie.path.replace(moviesBasePath, '/files/Movies'),
          filePath: path.join(movie.path, movie.filePath).replace(moviesBasePath, '/files/Movies'),
          landscape: movie.landscape
            ? path.join(movie.path, movie.landscape).replace(moviesBasePath, '/files/Movies')
            : null,
          fanart: movie.fanart
            ? path.join(movie.path, movie.fanart).replace(moviesBasePath, '/files/Movies')
            : null,
          thumbnail: movie.thumbnail
            ? path.join(movie.path, movie.thumbnail).replace(moviesBasePath, '/files/Movies')
            : null,
          subtitles: movie.subtitles.map(sub =>
            path.join(movie.path, sub).replace(moviesBasePath, '/files/Movies')
          )
        }))
      return res.status(222).send({ movies })
    } catch (err) {
      next(new HttpError(500, err))
    }
  })

  return router
}
