'use strict'

const ldap = require('ldapjs')
const crypto = require('crypto')

const sha256 = (data) => {
  const md = crypto.createHash('sha256')
  md.update(Buffer.from(data, 'utf8'))
  return md.digest().toString('hex')
}

let client
const getClient = () => {
  if (client) return client
  client = ldap.createClient({
    url: process.env.CLOUDRON_LDAP_URL,
    timeout: 10000, /* 10 seconds */
    reconnect: true /* undocumented option to automatically reconnect on connection failure : https://github.com/joyent/node-ldapjs/issues/318#issuecomment-165769581  */
  })
  return client
}

const bind = (dn, password) => new Promise((resolve, reject) => {
  console.log(`Doing LDAP auth for ${dn}`)
  getClient().bind(dn, password, err => err ? reject(err) : resolve())
})

const search = (username) => new Promise((resolve, reject) => {
  const filter = username
    ? { filter: `(|(uid=${username})(mail=${username})(username=${username})(sAMAccountName=${username}))` }
    : {}
  getClient().search(process.env.CLOUDRON_LDAP_USERS_BASE_DN, filter, (err, res) => {
    if (err) return reject(err)

    const entries = []
    let done = false
    res.on('searchEntry', entry => {
      if (done) return
      entries.push(entry.object)
    })
    res.on('error', err => {
      if (done) return
      done = true
      reject(err)
    })
    res.on('end', result => {
      if (done) return
      done = true
      if (result.status === 0) {
        resolve(entries)
      } else {
        reject(new Error('Unexpected error while retrieving users from LDAP. Status: ' + result.status))
      }
    })
  })
})

const cacheAuth = {}
const randomSalt = crypto.randomBytes(16).toString('hex') // random on each start

// clear cache regularly
setInterval(() => {
  for (const hash of Object.keys(cacheAuth)) {
    if (Date.now() - cacheAuth[hash].time >= 5 /* min */ * 60 /* seconds */ * 1000 /* milliseconds */) {
      delete cacheAuth[hash]
    }
  }
}, 10 /* min */ * 60 /* seconds */ * 1000 /* milliseconds */)

const auth = async (username, password) => {
  const hash = sha256(username + randomSalt + password) // Yes, this is weak, but it's only for in-memory cache so it's good enough

  if (cacheAuth[hash]) {
    if (Date.now() - cacheAuth[hash].time < 5 /* min */ * 60 /* seconds */ * 1000 /* milliseconds */) {
      if (cacheAuth[hash].data) return cacheAuth[hash].data
      else throw new Error('Wrong password')
    } // else the cache will be overwritten by this bind
  }

  const entries = await search(username)
  if (entries.length !== 1) {
    throw new Error('Unknown user')
  } else {
    try {
      const user = entries[0]
      await bind(`cn=${user.username},${process.env.CLOUDRON_LDAP_USERS_BASE_DN}`, password)
      const data = {
        id: user.uid,
        username: user.username,
        displayName: user.displayname,
        email: user.mail,
        admin: (user.isadmin && (user.isadmin.toLowerCase() === 'true' || user.isadmin === '1')) || // old isadmin flag, we need to parse string values sometimes...
          (Array.isArray(user.memberof) && user.memberof.includes(`cn=admins,${process.env.CLOUDRON_LDAP_GROUPS_BASE_DN}`)) // group-based approach for cloudron >= 5.2
      }
      cacheAuth[hash] = {
        data,
        time: Date.now()
      }
      return data
    } catch (err) {
      cacheAuth[hash] = {
        data: null,
        time: Date.now()
      }
      throw new Error('Wrong password')
    }
  }
}

const cacheList = {
  data: null,
  duration: /* 1 min: */ 60 /* seconds */ * 1000 /* milliseconds */, // User can change this value to modify duration of caching or disable with 0
  time: Date.now()
}

const listUsers = async (useCache = true) => {
  if (useCache && cacheList.data && Date.now() - cacheList.time < cacheList.duration) {
    return cacheList.data
  } else {
    const entries = await search()
    cacheList.time = Date.now() // remember the cache
    cacheList.data = entries
    return entries
  }
}

const doesUserExist = async (username, useCache = true) => {
  const users = await listUsers(useCache)
  return users.some(user => user.username === username)
}

module.exports = {
  auth,
  doesUserExist
}
