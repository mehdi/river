'use strict'

const fs = require('fs').promises
const path = require('path')
const tXml = require('txml')
const express = require('express')
const bodyParser = require('body-parser')
const HttpError = require('connect-lastmile').HttpError

module.exports = basePath => {
  const seriesBasePath = path.resolve(path.join(basePath, 'Series'))

  const router = new express.Router()

  const jsonParser = bodyParser.json()

  const getAbsolutePath = (filePath) => {
    const absoluteFilePath = path.resolve(path.join(seriesBasePath, filePath))

    if (!absoluteFilePath.startsWith(seriesBasePath)) return null
    return absoluteFilePath
  }

  const parseXml = async (filePath) => {
    const data = await fs.readFile(filePath, 'utf8')
    const parsed = tXml.simplify(tXml(data, {})) // the simplify option appears broken...
    return parsed['?xml']
  }

  const parseShowXml = async (filePath) => {
    const parsed = await parseXml(filePath)
    const metadata = parsed.tvshow
    return {
      title: metadata.title,
      rating: parseFloat(metadata.rating),
      year: parseInt(metadata.year),
      plot: metadata.plot,
      id: metadata.id,
      studio: metadata.studio,
      genre: typeof metadata.genre === 'string' ? [metadata.genre] : Array.from(metadata.genre || [])
    }
  }

  const parseEpisodeXml = async (filePath) => {
    const parsed = await parseXml(filePath)
    if (parsed.episodedetails) {
      const metadata = parsed.episodedetails
      return {
        title: metadata.title,
        season: parseInt(metadata.season),
        episode: parseInt(metadata.episode),
        id: metadata.uniqueid,
        aired: metadata.aired,
        plot: metadata.plot,
        rating: parseFloat(metadata.rating)
      }
    } else if (parsed.kodimultiepisode) {
      const episodes = parsed.kodimultiepisode.episodedetails
      return {
        title: episodes.map(e => e.title).join(' - '),
        season: parseInt(episodes[0].season),
        episode: episodes.map(e => e.episode).join('-'),
        id: episodes[0].uniqueid,
        aired: episodes[0].aired,
        plot: episodes.map(e => e.plot).join('\n\n'),
        rating: episodes.map(e => parseFloat(e.rating)).reduce((a, b) => a + b, 0) / episodes.length
      }
    }
  }

  const getShow = async (dirName) => {
    const showDirPath = getAbsolutePath(dirName)
    if (!showDirPath) return null
    const showDir = await fs.readdir(showDirPath, { withFileTypes: true })
    const showDirEntries = showDir.map(dirent => dirent.name)
    if (!showDirEntries.includes('tvshow.nfo')) return null
    const metadata = await parseShowXml(path.join(showDirPath, 'tvshow.nfo'))
    return {
      dirName,
      path: showDirPath,
      banner: showDirEntries.includes('banner.jpg') ? 'banner.jpg' : null,
      poster: showDirEntries.includes('poster.jpg') ? 'poster.jpg' : null,
      seasons: showDir
        .map(entry => {
          if (!entry.isDirectory()) return null
          const result = /^Season(?:[ .]|%20)?(\d+)$/.exec(entry.name)
          if (result) {
            return {
              seasonDir: entry.name,
              seasonNumber: result[1]
            }
          } else {
            return null
          }
        })
        .filter(r => r)
        .map(({ seasonNumber, seasonDir }) => ({
          path: path.resolve(showDirPath, seasonDir),
          number: parseInt(seasonNumber),
          poster: showDirEntries.includes(`season${seasonNumber}-poster.jpg`) ? `season${seasonNumber}-poster.jpg` : null
        })),
      title: metadata.title,
      rating: metadata.rating,
      year: metadata.year,
      plot: metadata.plot,
      id: metadata.id,
      studio: metadata.studio,
      genre: metadata.genre
    }
  }

  const getEpisode = async (seasonDirEntries, seasonDirPath, fileName) => {
    if (!fileName.endsWith('.mp4')) return null // TODO: also give non-streamable episodes
    const fileBaseName = fileName.slice(0, -4)
    if (!seasonDirEntries.includes(fileBaseName + '.nfo')) return null
    const metadata = await parseEpisodeXml(path.join(seasonDirPath, fileBaseName + '.nfo'))
    return {
      fileName,
      dirPath: seasonDirPath,
      filePath: path.join(seasonDirPath, fileName),
      title: metadata.title,
      season: metadata.season,
      episode: metadata.episode,
      id: metadata.id,
      aired: metadata.aired,
      plot: metadata.plot,
      rating: metadata.rating,
      thumbnail: seasonDirEntries.includes(fileBaseName + '-thumb.jpg') ? fileBaseName + '-thumb.jpg' : null,
      subtitles: seasonDirEntries.filter(entry => entry.startsWith(fileBaseName) && /\.(srt|vtt)$/.test(entry))
    }
  }

  router.post('/list_shows', async (req, res, next) => {
    try {
      const showDirs = (await fs.readdir(seriesBasePath, { withFileTypes: true }))
        .filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name)
      const shows = (await Promise.all(showDirs.map(entry => getShow(entry))))
        .filter(item => item) // filter out null
        .map(show => ({
          ...show,
          path: show.path.replace(seriesBasePath, '/files/Series'),
          seasons: show.seasons.map(season => ({
            path: season.path.replace(seriesBasePath, '/files/Series'),
            number: season.number,
            poster: season.poster
              ? season.poster.replace(seriesBasePath, '/files/Series')
              : null
          })),
          banner: show.banner
            ? path.join(show.path, show.banner).replace(seriesBasePath, '/files/Series')
            : null,
          poster: show.poster
            ? path.join(show.path, show.poster).replace(seriesBasePath, '/files/Series')
            : null
        }))
      return res.status(222).send({ shows })
    } catch (err) {
      next(new HttpError(500, err))
    }
  })

  router.post('/show_details', jsonParser, async (req, res, next) => {
    try {
      const { dirName } = req.body
      const show = await getShow(dirName)
      if (!show) return res.status(404).send('Not found')

      const seasons = (await Promise.all(show.seasons.map(async season => {
        const seasonDir = await fs.readdir(season.path)
        const episodes = await Promise.all(seasonDir.map(fileName => getEpisode(seasonDir, season.path, fileName)))
        return episodes.filter(episode => episode) // filter out null
      })))
        .reduce((a, b) => a.concat(b), []) // flatten
        .reduce((result, episode) => {
          result[episode.season] = result[episode.season] || {}
          result[episode.season][episode.episode] = Object.assign({}, episode, {
            filePath: episode.filePath.replace(seriesBasePath, '/files/Series'),
            dirPath: episode.dirPath.replace(seriesBasePath, '/files/Series'),
            thumbnail: episode.thumbnail
              ? path.join(episode.dirPath, episode.thumbnail).replace(seriesBasePath, '/files/Series')
              : null,
            subtitles: episode.subtitles.map(sub =>
              path.join(episode.dirPath, sub).replace(seriesBasePath, '/files/Series')
            )
          })
          return result
        }, {})

      const showPath = show.path.replace(seriesBasePath, '/files/Series')

      return res.status(222).send({
        ...show,
        seasons,
        path: showPath,
        banner: show.banner
          ? path.join(showPath, show.banner)
          : null,
        poster: show.poster
          ? path.join(showPath, show.poster)
          : null
      })
    } catch (err) {
      next(new HttpError(500, err))
    }
  })

  return router
}
