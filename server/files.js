'use strict'

const { constants, promises: fs } = require('fs')
const path = require('path')
const HttpError = require('connect-lastmile').HttpError
const { convertToMp4, canRemuxToMp4 } = require('../convert/utils.js')
const childProcess = require('child_process')
const { promisify } = require('util')
const express = require('express')
const bodyParser = require('body-parser')
const multipart = require('./multipart.js')

const execFile = promisify(childProcess.execFile)

module.exports = basePath => {
  basePath = path.resolve(basePath)

  const router = new express.Router()

  const isAdmin = (req, res, next) => (req.session && req.session.user && req.session.user.admin) ? next() : res.status(403).send('Unauthorized')

  const multipartParser = multipart({ maxFieldsSize: 2 * 1024, limit: '512mb', timeout: 3 * 60 * 1000 })
  const jsonParser = bodyParser.json()

  const getAbsolutePath = (filePath) => {
    const absoluteFilePath = path.resolve(path.join(basePath, filePath))

    if (!absoluteFilePath.startsWith(basePath)) return null
    return absoluteFilePath
  }

  const validateMmvPath = (input) => input && !input.startsWith('-') && !input.startsWith('/') && !input.startsWith('.')

  const escapeRegex = (text) => text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&')

  const removeBasepaths = (output, base = basePath) => output
    .replace(new RegExp(escapeRegex(base + '/'), 'g'), '')

  router.post('/canConvert', isAdmin, jsonParser, async (req, res, next) => {
    try {
      const { filePath } = req.body
      const fullPath = getAbsolutePath(filePath)
      if (!fullPath) return res.status(400).send('Bad Request, missing "filePath"')

      const canRemux = await canRemuxToMp4(fullPath)
      res.status(200).json({ canRemux })
    } catch (err) {
      next(new HttpError(500, err))
    }
  })

  router.post('/convert', isAdmin, jsonParser, async (req, res, next) => {
    try {
      const { filePath, force = false } = req.body
      const fullPath = getAbsolutePath(filePath)
      if (!fullPath) return res.status(400).send('Bad Request, missing "filePath"')

      await convertToMp4(fullPath, { force })
      res.status(200).json({ done: true })
    } catch (err) {
      next(new HttpError(500, err))
    }
  })

  router.post('/mmv', isAdmin, jsonParser, async (req, res, next) => {
    try {
      const { dirPath, from, to, dryRun = true } = req.body

      const fullDirPath = getAbsolutePath(dirPath)
      const fullFrom = path.join(fullDirPath, from)
      const fullTo = path.join(fullDirPath, to)

      if (!fullDirPath) return res.status(400).send('Bad Request, invalid "dirPath"')
      if (!fullFrom || !validateMmvPath(from)) return res.status(400).send('Bad Request, invalid "from"')
      if (!fullTo || !validateMmvPath(to)) return res.status(400).send('Bad Request, invalid "to"')

      const stat = await fs.stat(fullDirPath)

      if (!stat.isDirectory()) return res.status(409).send('"dirPath" is not a directory')

      const options = [
        '-t', // No interactive
        '-p' // No overwrite
      ]

      if (dryRun) options.push('-n')

      options.push(fullFrom)
      options.push(fullTo)

      try {
        const { stdout, stderr } = await execFile('mmv', options)
        res.status(201).json({
          stdout: removeBasepaths(stdout, fullDirPath),
          stderr: removeBasepaths(stderr, fullDirPath)
        })
      } catch (err_) {
        res.status(500).json({
          err: removeBasepaths(err_.message, fullDirPath),
          stdout: removeBasepaths(err_.stdout, fullDirPath),
          stderr: removeBasepaths(err_.stderr, fullDirPath)
        })
      }
    } catch (err) {
      next(new HttpError(500, err))
    }
  })

  router.post('/delete', isAdmin, jsonParser, async (req, res, next) => {
    try {
      const { filePath } = req.body

      const absoluteFilePath = getAbsolutePath(filePath)
      if (!absoluteFilePath || absoluteFilePath === basePath) return res.status(404).send('Not found')

      const stat = await fs.stat(absoluteFilePath)

      if (stat.isDirectory()) await fs.rmdir(absoluteFilePath, { recursive: true })
      else await fs.unlink(absoluteFilePath)

      res.status(200).json({ status: 'ok' })
    } catch (err) {
      next(new HttpError(500, err))
    }
  })

  router.post('/move', isAdmin, jsonParser, async (req, res, next) => {
    try {
      const { from, to } = req.body

      const fullFrom = getAbsolutePath(from)
      const fullTo = getAbsolutePath(to)

      if (!fullFrom) return res.status(400).send('Bad Request, invalid "from"')
      if (!fullTo || fullTo.startsWith(fullFrom)) return res.status(400).send('Bad Request, invalid "to"')

      try { // we need to assert the destination does not exist
        await fs.stat(fullTo)
        return res.status(409).send('Already exists')
      } catch (err_) { // this is expected
        if (err_.code !== 'ENOENT') throw err_ // unexpected error, rethrow
        await fs.rename(fullFrom, fullTo)
        return res.status(201).json({})
      }
    } catch (err) {
      next(new HttpError(500, err))
    }
  })

  router.post('/create_directory', isAdmin, jsonParser, async (req, res, next) => {
    try {
      const { dirPath } = req.body

      const fullDirPath = getAbsolutePath(dirPath)

      if (!fullDirPath) return res.status(400).send('Bad Request, invalid "dirPath"')

      try {
        await fs.mkdir(fullDirPath)
        return res.status(201).json({})
      } catch (err_) {
        if (err_.code === 'EEXIST') return res.status(409).send('Already exists')
        else throw err_ // unexpected error, rethrow
      }
    } catch (err) {
      next(new HttpError(500, err))
    }
  })

  router.post('/list', jsonParser, async (req, res, next) => {
    try {
      const { dirPath } = req.body

      const fullDirPath = getAbsolutePath(dirPath)

      if (!fullDirPath) return res.status(400).send('Bad Request, invalid "dirPath"')

      const dirEntries = await fs.readdir(fullDirPath)
      const results = await Promise.all(dirEntries.map(async (entry) => {
        const stat = await fs.stat(path.join(fullDirPath, entry))
        return {
          isDirectory: stat.isDirectory(),
          isFile: stat.isFile(),
          atime: stat.atime,
          mtime: stat.mtime,
          ctime: stat.ctime,
          birthtime: stat.birthtime,
          size: stat.size,
          filePath: entry
        }
      }))
      return res.status(222).json({ entries: results })
    } catch (err) {
      next(new HttpError(500, err))
    }
  })

  router.post('/upload', isAdmin, multipartParser, async (req, res, next) => {
    try {
      if (!req.files.file || !req.files.file.path) return res.status(400).send('Bad Request, invalid "file"')

      const tempPath = req.files.file ? req.files.file.path : null
      const destPath = getAbsolutePath(req.fields.filePath)

      if (!tempPath) return res.status(400).send('Bad Request, invalid "file"')
      if (!destPath) return res.status(400).send('Bad Request, invalid "path"')

      try {
        // we need to copy instead of rename, because it will probably be on different device
        await fs.copyFile(tempPath, destPath, constants.COPYFILE_EXCL) // COPYFILE_EXCL flag to throw EEXIST if dest exists
        return res.status(201).json({})
      } catch (err_) { // this is expected
        if (err_.code === 'EEXIST') return res.status(409).send('Already exists')
        else throw err_
      } finally {
        await fs.unlink(tempPath) // cleanup
      }
    } catch (err) {
      next(new HttpError(500, err))
    }
  })

  return router
}
