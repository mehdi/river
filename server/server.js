#!/usr/bin/env node

'use strict'

const express = require('express')
const morgan = require('morgan')
const path = require('path')
const compression = require('compression')
const session = require('express-session')
const lastMile = require('connect-lastmile')
const bodyParser = require('body-parser')
const fs = require('fs')
const Files = require('./files.js')
const Series = require('./series.js')
const Movies = require('./movies.js')
const contentDisposition = require('content-disposition')
const ldap = require('./ldap.js')
const { localAuth } = require('./localAuth.js')
const LokiStore = require('connect-loki')(session)
const { exec } = require('child_process')
const { promisify } = require('util')
const basicAuth = require('basic-auth')

const execAsync = promisify(exec)

const app = express()

const isRunning = async (name) => {
  try {
    return (await execAsync('supervisorctl status ' + name))
      .stdout
      .includes('RUNNING')
  } catch (err) {
    return false
  }
}

const onCloudron = Boolean(process.env.CLOUDRON)

const jsonParser = bodyParser.json()

const authenticate = async (username, password) => {
  try {
    if (useLdap) return await ldap.auth(username, password)
    else return await localAuth(username, password)
  } catch (err) {
    return null
  }
}

const isAuthenticated = (req, res, next) => (req.session && req.session.user) ? next() : res.status(401).send('Unauthorized')

const baseDir = onCloudron ? '/app/data' : path.resolve('./data')
const filesPath = path.resolve(baseDir, 'files')

app.use('/api/healthcheck', async (req, res) => {
  const services = ['transmission', 'sickchill', 'jellyfin', 'couchpotato']
  for (const service of services) {
    if (!(await isRunning(service))) {
      console.warn(`Service ${service} is not running`)
      return res
    }
  }
  res.status(200).send('running')
})
app.use(morgan('dev'))
app.use(compression())
if (onCloudron) app.set('trust proxy', 1)
app.use(session({
  secret: fs.readFileSync(path.resolve(baseDir, 'session.secret'), 'utf8'),
  store: new LokiStore({
    path: path.resolve(baseDir, 'session.db'),
    logErrors: true
  }),
  resave: false,
  saveUninitialized: false,
  name: 'river.sid',
  cookie: {
    path: '/',
    httpOnly: true,
    secure: onCloudron,
    sameSite: 'strict',
    maxAge: /* 1 year: */ 365 /* d */ * 24 /* h */ * 60 /* min */ * 60 /* s */ * 1000 /* ms */
  }
}))

app.get('/api/check-login', async (req, res) => {
  if (req.session && req.session.user) {
    return res.status(200).send({
      username: req.session.user.username,
      admin: req.session.user.admin
    })
  } else {
    const credentials = basicAuth(req)
    if (credentials && await authenticate(credentials.name, credentials.pass)) {
      return res.status(200).send('ok')
    } else {
      return res.status(401).send('Unauthorized')
    }
  }
})

app.get('/api/check-admin', async (req, res) => {
  if (req.session && req.session.user) {
    if (req.session.user.admin) {
      return res.status(200).send({
        username: req.session.user.username,
        admin: req.session.user.admin
      })
    } else return res.status(403).send('Unauthorized')
  } else {
    const credentials = basicAuth(req)
    if (credentials) {
      const user = await authenticate(credentials.name, credentials.pass)
      if (user && user.admin) return res.status(200).send('ok')
      else if (user) return res.status(403).send('Unauthorized')
    }
    return res.status(401).send('Unauthorized')
  }
})

const useLdap = Boolean(process.env.CLOUDRON_LDAP_URL)

app.post('/api/login', jsonParser, async (req, res, next) => {
  if (!req.body.username || !req.body.password) {
    req.session.user = null
    res.status(401).send('Unauthorized')
  } else {
    const user = await authenticate(req.body.username, req.body.password)
    if (user) {
      req.session.user = user
      res.status(200).send({ username: req.session.user.username, admin: req.session.user.admin })
    } else {
      res.status(401).send('Unauthorized')
    }
  }
})

app.get('/api/logout', (req, res) => {
  req.session.user = null
  res.redirect('/')
})

app.use('/api/series/', isAuthenticated, Series(filesPath))
app.use('/api/movies/', isAuthenticated, Movies(filesPath))

app.use('/api/files/', isAuthenticated, Files(filesPath))

if (!onCloudron) {
  app.use('/files', isAuthenticated, express.static(filesPath, {
    index: false,
    setHeaders: (res, path) => res.setHeader('Content-Disposition', contentDisposition(path))
  }))
}
app.use('/', express.static(path.resolve(__dirname, '../client')))
app.use(lastMile())

const server = app.listen(3000, function () {
  const { host, port } = server.address()

  fs.mkdirSync(filesPath, { recursive: true })

  if (!onCloudron) console.warn('NOT running on Cloudron! Using local auth')
  console.log('River listening at http://%s:%s', host, port)
  console.log('Using base path', filesPath)
})
