# River

River is an app that packages together:
- Jellyfin, a streaming platform, like your own self-hosted Netflix
- SickChill, to auto-download TV shows
- CouchPotato, to auto-download movies
- Transmission, to handle actually downloading things
- A simple file manager, when you need to handle things manually (create directories, move files by drag&drop, upload, rename, delete, download files, stream videos)

Basically, just add the TV Shows you want in SickChill, add the movies you want in CouchPotato, and watch everything on Jellyfin.

- files interface: `/`
- Jellyfin: `/jellyfin/`
- Transmission web interface (admin only): `/transmission/web/`
- SickChill (admin only): `/sickchill/`
- Couchpotato (admin only): `/couchpotato/`

There are also some conversion scripts that convert things downloaded to .mp4 for easy streaming, if such conversion
is possible with no transcoding.

## Installation

[![Install](https://cloudron.io/img/button32.png)](https://cloudron.io/button.html?app=io.cloudron.river)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id io.cloudron.river
```

## Building

### Cloudron
The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
git clone https://git.cloudron.io/mehdi/river.git
cd river
cloudron build
cloudron install
```
