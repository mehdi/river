#!/usr/bin/env node
'use strict'

const fs = require('fs')

// language=XML
const config = `<?xml version="1.0"?>
<PluginConfiguration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <LdapServer>${process.env.CLOUDRON_LDAP_SERVER}</LdapServer>
    <LdapBaseDn>${process.env.CLOUDRON_LDAP_USERS_BASE_DN}</LdapBaseDn>
    <LdapPort>${process.env.CLOUDRON_LDAP_PORT}</LdapPort>
    <LdapSearchAttributes>username, mail</LdapSearchAttributes>
    <LdapUsernameAttribute>username</LdapUsernameAttribute>
    <LdapSearchFilter>(objectclass=user)</LdapSearchFilter>
    <LdapAdminFilter>(memberof=cn=admins,${process.env.CLOUDRON_LDAP_GROUPS_BASE_DN})</LdapAdminFilter>
    <LdapBindUser>${process.env.CLOUDRON_LDAP_BIND_DN}</LdapBindUser>
    <LdapBindPassword>${process.env.CLOUDRON_LDAP_BIND_PASSWORD}</LdapBindPassword>
    <CreateUsersFromLdap>true</CreateUsersFromLdap>
    <UseSsl>false</UseSsl>
</PluginConfiguration>
`

fs.mkdirSync('/app/data/jellyfin/data/plugins/configurations/', { recursive: true })
fs.writeFileSync('/app/data/jellyfin/data/plugins/configurations/LDAP-Auth.xml', config)
