#!/usr/bin/env node
'use strict'

const fs = require('fs')
const mimeTypesStandard = require('mime/types/standard.js')
const mimeTypesOthers = require('mime/types/other.js')
const mimeTypes = Object.assign({}, mimeTypesStandard, mimeTypesOthers)

const mimeTypesToStr = types => {
  let res = 'types {\n'
  for (const type in types) {
    res += `    ${type}    ${types[type].map(t => t.replace('*', '')).join(' ')};\n`
  }
  res += '}\n'
  return res
}

// language=nginx
const nginxConf = `user cloudron;
worker_processes 4;
worker_priority -10;
pid /run/nginx.pid;
daemon off;

# Send logs to stderr
error_log /dev/stderr warn;

events {
    worker_connections 768;
}

http {
    error_log /dev/stderr warn;
    log_format simple '$remote_addr [$time_local] "$request" $status $body_bytes_sent "$http_referer"';
    access_log /dev/stdout simple;

    types_hash_max_size 2048;

    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    client_body_temp_path /tmp/client_body 1 2;
    client_body_buffer_size 256k;
    client_body_in_file_only off;
    client_max_body_size 200M;

    proxy_buffering off;
    proxy_cache off;

    proxy_temp_path /tmp/proxy_temp 1 2;
    fastcgi_temp_path /tmp/fastcgi_temp 1 2;
    uwsgi_temp_path /tmp/uwsgi_temp 1 2;
    scgi_temp_path /tmp/scgi_temp 1 2;

    server {
        error_log /dev/stderr warn;
        listen 8000 default_server;

        server_name _;

        error_page 401 = @error401;
        location @error401 {
            return 302 " /#login?redirect=$request_uri";
        }

        location = /check-sso-login {
            internal;
            proxy_pass http://localhost:3000/api/check-login;
            proxy_pass_request_body off;
            proxy_set_header Content-Length "";
        }

        location = /check-sso-admin {
            internal;
            proxy_pass http://localhost:3000/api/check-admin;
            proxy_pass_request_body off;
            proxy_set_header Content-Length "";
        }

        location /files/ {
            # Authentication
            auth_request /check-sso-login;

            # Static files server
            root /app/data;
            autoindex on;
            sendfile on;
            sendfile_max_chunk 1m;
            tcp_nopush on;
            if ($request_filename ~ "^.*/([^/]+)$"){
                add_header Content-Disposition 'attachment; filename="$1"';
            }
            default_type application/octet-stream;
            ${mimeTypesToStr(mimeTypes)}
        }

        location /transmission/ {
            auth_request /check-sso-admin;
            proxy_pass http://localhost:9091;
        }
        
        location /sickchill/ {
            auth_request /check-sso-admin;
            # SickChill decided to be stupid & prevent "unauthenticated" access without any override possible
            # So we just make it believe that it's purely a local connexion
            proxy_set_header X-Real-Ip "";
            proxy_set_header X-Forwarded-For "";
            proxy_pass http://localhost:8081;
        }

        location /couchpotato/ {
            auth_request /check-sso-admin;
            proxy_pass http://localhost:5050;
        }

        location /jellyfin/ {
            proxy_pass http://localhost:8096;
        }

        location / {
            proxy_pass http://localhost:3000;
        }
    }
}
`

fs.writeFileSync('/run/blank', '')
fs.writeFileSync('/run/nginx.conf', nginxConf)
